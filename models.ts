// export interface Meta {
//   totalCount: number;
// }

export interface ISlides {
  title: string;
  description?: string;
  link: string;
  image: string;
}

export interface IErrorImage {
  failedPropValidation?: string
  file?: object
}

export interface IImage {
  id: string
  name?: string
  url: string
  created_at?: string
  updated_at?: string
}

export interface ICategory {
  id?: number | null
  name: string | null
  slug?: string | null
  created_at?: string | null
  updated_at?: string | null
}
 
export interface IGallery {
  id: string
  image: IImage
}

export interface IOption {
  id: number
  value: string
}

export interface IAttribute {
  id?: number
  name: string
  lower: string
  slug: string
  created_at?: string
  updated_at?: string
}

export interface ITag {
  id?: number
  name: string
  lower: string
  slug: string
  created_at?: string
  updated_at?: string
}

export interface IParam {
  id: number
  short_url: string
  option: IOption
  value: string
  description?: string
  attribute?: IAttribute
}

export interface IUser {
  id?: number | null
  password?: string | null
  email?: string | null
  is_admin?: boolean | null
  avatarPath?: string | null
  name?: string | null
  phone?: string | null
  created_at?: string | null
  updated_at?: string | null
}

export interface IPagination {
  page: number
  perPage: number
}

export interface IAnswer {
  id: string
  user: IUser
  text: string
}

export interface IReview {
  id: string
  user: IUser
  text: string
  answers?: IAnswer
  rating?: Number
}

export interface createReview {
  id: number
  rating: number
  text: string
}

export interface IProduct {
  id?: number | null
  name: string | null
  slug?: string | null
  category?: ICategory | null
  image?: IImage | null
  images?: IImage[] | []
  gallery?: { id: string; image: IImage }[] | null
  price: number | null
  description?: string | null
  params?: IParam | null
  reviews?: IReview[] | null
  created_at?: string | null
  updated_at?: string | null
}

export interface IProductsCart {
  price: number,
  productId: number,
  quantity: number,
  slug?: string | null,
  name?: string | null,
  image?: string | null | undefined 
}

export interface IQuery {
  [key: string]: any
  searchTerm?: string | null
  sort?: string | null
  categoryId?: number | null
  min?: number | null
  max?: number | null
}
