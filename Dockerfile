FROM node:18.15.0-alpine3.17

WORKDIR /app

COPY . .

RUN yarn install
RUN yarn build

EXPOSE 3000

CMD [ "yarn", "preview" ]