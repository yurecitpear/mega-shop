import { defineStore } from 'pinia'
import { IProduct, IProductsCart } from "@/models"

interface state {
  products: IProductsCart[]
}

export const useCartStore = defineStore({
  id: 'cart',
  state: (): state => ({
    products: []
  }),

  actions: {
    async addInCart(product: IProductsCart) {
      const productInCart = this.products.find(p => p.productId === product.productId)
      console.log(productInCart)
      if (productInCart) {
        productInCart.quantity++
      } else {
        const productObj: IProductsCart  = {
          productId: Number(product.id),
          price: Number(product.price),
          quantity: 1,
          name: product.name,
          slug: product.slug,
          image: String(product?.images ? product?.images[0] : null)
        }
        this.products.push(productObj)
      }
    },

    async removeFromCart(id: number) {
      this.products = this.products.filter(p => p.productId!== id)
    },

    async removeAllFromCart() {
      this.products = []
    },

    async MinusQuantityProduct(product: IProductsCart) {
      const productInCart = this.products.find(p => p.productId === product.productId)
      if (productInCart) {
        if (productInCart.quantity > 1) {
          productInCart.quantity--
        } else {
          this.products = this.products.filter(p => p.productId!== product.productId)
        }
      }
    }
  },

  getters: {
    getCart(): IProductsCart[] {
      return this.products
    },
    getQuantityTotal(): number {
      let total = 0
      this.products.forEach(p => {
        total += p.quantity
      })
      return total
    },
    getTotal(): number {
      let total = 0
      this.products.forEach(p => {
        total += p.price * p.quantity
      })
      return total
    }
  },
  
  persist: {
    storage: persistedState.localStorage,
  },
})
