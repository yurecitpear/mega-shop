import { defineStore } from 'pinia'

interface State {
  catalogView: string
}

export const useQueryStore = defineStore({
  id: 'query',
  state: (): State => ({
    catalogView: 'colummn',
  }),

  actions: {
    async changeCatalogView(val: string) {
      this.catalogView = val
    },
  },
  
  persist: {
    storage: persistedState.localStorage,
  },
})
