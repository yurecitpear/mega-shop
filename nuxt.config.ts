import { Notify } from "quasar"


// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: [
    "quasar/dist/quasar.css",
    "@/assets/css/fonts.css",
    "@/assets/css/bootstrap.min.css",
    "@/assets/css/magnific-popup.min.css",
    "@/assets/css/fontawesome.min.css",
    "@/assets/css/jquery.fancybox.min.css",
    "@/assets/css/jquery-ui.css",
    "@/assets/css/themify-icons.css",
    "@/assets/css/flaticon.css",
    "@/assets/css/niceselect.css",
    "@/assets/css/animate.css",
    "@/assets/css/flex-slider.min.css",
    // "@/assets/css/owl-carousel.css",
    // "@/assets/css/slicknav.min.css",
    "@/assets/css/default.css",
    "@/assets/css/style.css",
    "@/assets/css/responsive.css",
    "@/assets/css/my-style.scss",
    "@quasar/extras/material-icons/material-icons.css",
    
  ],
  modules: [
    'nuxt-quasar-ui',
    '@pinia/nuxt',
    '@pinia-plugin-persistedstate/nuxt'
  ],
  quasar: {
    sassVariables: '@/assets/css/quasar.variables.scss',
    plugins: ['Notify'],
  },

})
