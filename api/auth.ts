import httpClient from "./httpClient.js"
import { IUser } from '@/models'
import Cookies from 'js-cookie'

const url = 'auth'

function setAuthData(data: { accessToken: string, user: object }): void {
  localStorage.setItem('token', JSON.stringify(data.accessToken))
  localStorage.setItem('user', JSON.stringify(data.user))
  Cookies.set('token', data.accessToken)
}

export const authApi = {
  
  async doLogin(data: object): Promise<IUser[]> {
    try {
      const resp = await httpClient.post(`${url}/login`, data)
      setAuthData(resp.data)
      return resp.data.data
    } catch (err) {
      throw err
    }
  },

  async doRegister(data: object): Promise<IUser> {
    try {
      const resp = await httpClient.post(`${url}/register`, data)
      setAuthData(resp.data)
      return resp.data.data
    } catch (err) {
      throw err
    }
  },

}