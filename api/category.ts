import httpClient from "./httpClient.js"
import { ICategory } from '@/models'
import { ICategoryFrom } from '@/types/CategoryModel'

const url = 'categories'

export const categoryApi = { 

  async getRecommend(): Promise<ICategory[]> {
    try {
      const resp = await httpClient.get(`${url}?isRecommended=true`)
      return resp.data
    } catch (err) {
      throw err
    }
  },
  
  async getAll(): Promise<ICategory[]> {
    try {
      const resp = await httpClient.get(`${url}`)
      return resp.data
    } catch (err) {
      throw err
    }
  },

  async create(data: ICategoryFrom, image: string): Promise<ICategory> {
    try {
      const formData = new FormData()
      formData.append("name", String(data.name))
      formData.append("isRecommended", String(data.isRecommended))
      formData.append(`image`, image)

      const resp = await httpClient.post(`${url}`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      return resp.data
    } catch (err) {
      throw err
    }
  }
  
}