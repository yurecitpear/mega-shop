import axios from "axios";
import Cookies from 'js-cookie'

function logOut() {
  localStorage.clear()
  window.location.href = '/login'
  location.reload()
  Cookies.set('token', '')
}

const config: object = {
  baseURL: "http://localhost:4200/api/"
}

let token: string | null = null

const httpClient = axios.create(config);

if (process.client) {
  token = localStorage.getItem('token') || ''
  token = token.replace(/"/g, '')
}
if (process.server) {
  token = Cookies.get('token') || ''
  token = token.replace(/"/g, '')
}


if (token) {
  httpClient.interceptors.request.use(
    (config) => {
      if (config.headers) {
        config.headers.Authorization = `Bearer ${token}`
      } else {
        config.headers = { Authorization: `Bearer ${token}`}
      }
      return config
    }
  )
  
  httpClient.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    const err = error.response.status
  
    const originalRequest = error.config;
    if (originalRequest && !originalRequest._isRetry) {
      originalRequest._isRetry = true;
  
      if(err === 401) {
        console.log('Неавторизован')
        logOut() 
      }
      if(err === 403) {
        console.log('Нет доступа')
      }
      if(err === 404) {
        console.log('Не найдено')
      }
    }
    return Promise.reject(error)
  })
}




export default httpClient