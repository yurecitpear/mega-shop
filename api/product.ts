import httpClient from "./httpClient.js"
import { IProductFrom } from '@/types/ProductModel' 
import { IProduct, IQuery, IPagination } from '@/models'
import { usePagination } from '@/composables/useQuery'
 
const url = 'products' 

interface IApiParameters extends IPagination {
  sort?: string;
  categoryId?: number;
  min?: number; 
  max?: number;  
}

export const productApi = {

  async getRecommend(): Promise<{ products: IProduct[]; length: number }> {
    try {
      const resp = await httpClient.get(`${url}?isRecommended=true`)
      return resp.data
    } catch (err) {
      throw err
    }
  },

  async getByCategory(categoryId: number): Promise<{ products: IProduct[]; length: number }> {
    try {
      const resp = await httpClient.get(`${url}?categoryId=${categoryId}&page=1&perPage=9`)
      return resp.data
    } catch (err) {
      throw err
    }
  },

  async get(pagination: IPagination, query: IQuery): Promise<{ products: IProduct[]; length: number }> {
    const params = new URLSearchParams()

    params.set('page', pagination.page.toString())
    params.set('perPage', pagination.perPage.toString())
    
    const keyMap: { [key in keyof IQuery]: string } = {
      sort: 'sort',
      categoryId: 'categoryId',
      min: 'minPrice',
      max: 'maxPrice',
      searchTerm: 'searchTerm'
    }
    for (const key in query) {
      if (query[key]) {
        params.set(keyMap[key as keyof IQuery], query[key].toString())
      }
    }

    try {
      const resp = await httpClient.get(`${url}?${params.toString()}`)
      return resp.data
    } catch (err) {
      throw err
    }
  },

  async getBySlug(slug: string): Promise<IProduct> {
    try {
      const resp = await httpClient.get(`${url}/by-slug/${slug}`)
      return resp.data
    } catch (err) {
      throw err
    }
  },

  async getSimilar(productId: number): Promise<IProduct[]> {
    try {
      const resp = await httpClient.get(`${url}/similar/${productId}`)
      return resp.data
    } catch (err) {
      throw err
    }
  },
  
  // async create(data: IProductFrom) {
  //   const formData = new FormData()
  //   formData.append("name", String(data.name))
  //   formData.append("description", String(data.description))
  //   formData.append("category_id", String(data.category_id))

  //   try {
  //     const resp = await httpClient({
  //       method: "post",
  //       url: `${url}/update`,
  //       data: formData,
  //       headers: { "Content-Type": "multipart/form-data" },
  //     })
  //     return resp.data
  //   } catch (err) {
  //     throw err
  //   }
  // },

  async create(data: IProductFrom, image: File, images: File[]): Promise<IProduct> {
    const formData = new FormData()
    formData.append("name", String(data.name))
    formData.append("description", String(data.description))
    formData.append("categoryId", String(data.category_id.id))
    formData.append("price", String(data.price))
    if (image) formData.append("image", image)
    
    const formImages = new FormData()
      if (images && images.length > 0) {
      images.forEach((image) => {
        formImages.append(`images`, image)
      })   
    }

    try {
      const resp = await httpClient.post(`${url}`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      console.log(resp.data)
      
      const images = await httpClient.post(`${url}/${resp.data.id}/upload-multiple`, formImages, {
        headers: { "Content-Type": "multipart/form-data" },
      })

      console.log(images)

      return resp.data
    } catch (err) {
      throw err
    }
  }

  // async getById(slug: string): Promise<IProduct> {
  //   try {
  //     return await Promise.resolve(product)
  //   } catch (err) {
  //     console.log(err)
  //     throw err
  //   }
  // }

}