import httpClient from "./httpClient.js"
import { IProductFrom } from '@/types/ProductModel' 
import { IReview, createReview } from '@/models'
import Cookies from 'js-cookie'

const url = 'reviews' 

export const reviewsApi = {

  async get(): Promise<{ reviews: IReview[]}> {
    try {
      const resp = await httpClient.get(url)
      return resp.data
    } catch (err) {
      throw err
    }
  },
  
  async create(data: createReview): Promise<{ reviews: IReview}>  {
    try {
      const resp = await httpClient.post(`${url}/leave/${data.id}`, {
        rating: data.rating,
        text: data.text
      })
      return resp.data
    } catch (err) {
      throw err
    }
  },

}