import { useRouter } from 'vue-router'

export default defineNuxtRouteMiddleware((to) => {
  const cookieToken: any = useCookie('token')
  const router = useRouter()
  let token: string | null = ''


  // if (process.server) {
  //   token = cookieToken.value
  //   if (!token) {
  //     localStorage.clear()
  //     return router.push('/auth/login')
  //   }
  // }

  if (process.client) {
    token = localStorage.getItem('token')
    if (!token) {
      localStorage.clear();
      return router.push('/auth/login')
    }
  }
})