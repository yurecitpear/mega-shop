export interface ICategoryFrom {
  name: string
  isRecommended: boolean
}
