import { IProduct, IPagination } from '@/models'

export interface IProductFrom {
  id?: number | null
  name: string | null
  description?: string | null
  category_id: number | null
  images?: File | null
  price: number | null
  attributes?: number[] | null
}

export interface IProductResponse {
  id?: number | null
  data?: IProduct[] | null
  meta?: IPagination | null
  status?: string | null
  message?: string | null
}