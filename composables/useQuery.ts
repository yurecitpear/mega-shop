import { IPagination } from '@/models'

function usePagination(pagination: IPagination): string | null {
  let query = '?'  

  for (let [key, value] of Object.entries(pagination)) {
    if (value !== null && value !== undefined) { 
      query += `${encodeURIComponent(key)}=${encodeURIComponent(value.toString())}&`
    }
  }

  return query.slice(0, -1)
}

export { usePagination }