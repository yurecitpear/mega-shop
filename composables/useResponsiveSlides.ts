import { ref, onMounted, onUnmounted, Ref } from 'vue'

export default function useResponsiveSlides(defaultItems: number): Ref<number> {
  const itemsToShow: Ref<number> = ref(defaultItems)

  onMounted(() => {
    const mediaQuery = window.matchMedia('(max-width: 772px)')
  
    const handleResize = (): void => {
      itemsToShow.value = mediaQuery.matches ? 1 : defaultItems
    }

    handleResize()
    mediaQuery.addListener(handleResize)
  
    onUnmounted(() => {
      mediaQuery.removeListener(handleResize)
    })
  })

  return itemsToShow
}