import { ref, onMounted, onBeforeUnmount } from 'vue'

export default function useScreenSize() {
  let size = 'lg'
  if (typeof window !== 'undefined') {
    if (window.innerWidth < 772) {
      size = 'sm'
    } else if (window.innerWidth < 1024) {
      size = 'md'
    }
  }
  const screenSize = ref(size)

  const handleResize = () => {
    if (window.innerWidth < 772) {
      screenSize.value = 'sm'
    } else if (window.innerWidth < 1024) {
      screenSize.value = 'md'
    } else {
      screenSize.value = 'lg'
    }
  }

  onMounted(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('resize', handleResize)
    }
  })

  onBeforeUnmount(() => {
    if (typeof window !== 'undefined') {
      window.removeEventListener('resize', handleResize)
    }
  })

  return {
    screenSize
  }
}