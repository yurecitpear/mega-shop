function setItemLocalStorage(key: string, value: any) {
  if (typeof localStorage !== 'undefined') {
    localStorage.setItem(key, JSON.stringify(value))
  }
}

function getItemLocalStorage(key: string) {
  if (typeof localStorage !== 'undefined') {
    const data = localStorage.getItem(key)
    return data ? JSON.parse(data) : null
  }
  return null
}

export { setItemLocalStorage, getItemLocalStorage }